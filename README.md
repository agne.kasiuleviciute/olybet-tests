# Introduction

Oly Bet tests for: 

  -  Login
  - Change language to English
  - Navigate to "My Profile" page -> "Information" tab (https://www.olybet.lt/user/profile)
  - Verify user details
  - Change password
  - Logout
